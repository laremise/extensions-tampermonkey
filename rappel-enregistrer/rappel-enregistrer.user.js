// ==UserScript==
// @name         Rappel d'enregistrer sur MyTurn
// @namespace    http://tampermonkey.net/
// @version      0.1
// @author       Simon Marchi
// @match        https://montreal.myturn.com/library/orgLoan/startCart*
// @grant        none
// ==/UserScript==

function makeBanner() {
    return '<marquee style="font-size: 5em; color: #8E44AD;">Ne pas oublier d\'appuyer sur "Enregistrer" après avoir fait les modifications!</marquee>';
}

(function() {
    'use strict';

    $(makeBanner()).insertBefore($('#cart-form'));

    $('#cart-summary').hide(0);
    $('#cart-payments').hide(0);

    console.log('mario!');
})();
