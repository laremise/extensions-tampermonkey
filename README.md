Installation
============

1. Installer Tampermonkey
   ([Firefox](https://addons.mozilla.org/en-CA/firefox/addon/tampermonkey/),
   [Chromium](https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en))
2. Créer un nouveau script: cliquer sur l'icône Tampermonkey et choisir `Create
   a new script...`.
3. Copier/coller la source du script voulu.
